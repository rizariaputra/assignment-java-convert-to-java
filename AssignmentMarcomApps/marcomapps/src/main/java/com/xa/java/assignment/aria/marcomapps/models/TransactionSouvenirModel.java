package com.xa.java.assignment.aria.marcomapps.models;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PostPersist;
import javax.persistence.Table;

@Entity
@Table(name="t_souvenir")
public class TransactionSouvenirModel {
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", nullable = false, length = 11)
    private Integer id;
    
    @Column(name="code", nullable = false, length = 50)
    private String code;

    @PostPersist
    public void GenerateSouvenirCode() {
        String datecode = LocalDate.now().toString();
        if(id.toString().length() == 1) code = ("TRSV" + datecode + "0000" + id);
        else if(id.toString().length() == 2) code = ("TRSV" + datecode + "000" + id);
        else if(id.toString().length() == 3) code = ("TRSV" + datecode + "00" + id);
        else if(id.toString().length() == 4) code = ("TRSV" + datecode + "0" + id);
    }
    
    @Column(name="type", nullable = false, length = 11)
    private String type;

    @Column(name="t_event_id", nullable = true, length = 50)
    private Integer tEventId;

    @Column(name="request_by", nullable = false, length = 50)
    private Integer requestBy;

    @Column(name="request_date", nullable = true)
    private LocalDateTime requestDate;

    @Column(name="request_due_date", nullable = true)
    private LocalDateTime requestDueDate;
    
    @Column(name="approved_by", nullable = true, length = 50)
    private Integer approvedBy;

    @Column(name="approved_date", nullable = true)
    private LocalDateTime approvedDate;

    @Column(name="received_by", nullable = true, length = 50)
    private Integer receivedBy;

    @Column(name="received_date", nullable = true)
    private LocalDateTime receivedDate;

    @Column(name="settlement_by", nullable = true, length = 50)
    private Integer settlementBy;

    @Column(name="settlement_date", nullable = true)
    private LocalDateTime settlementDate;

    @Column(name="settlement_approved_by", nullable = true, length = 50)
    private Integer settlementApprovedBy;

    @Column(name="settlement_approved_date", nullable = true)
    private LocalDateTime settlementApprovedDate;

    @Column(name="status", nullable = true, length = 1)
    private Integer status;

    @Column(name="note", nullable = true, length = 255)
    private String note;

    @Column(name="reject_reason", nullable = true, length = 255)
    private String rejectReason;

    @Column(name="is_delete", nullable = true)
    private Boolean isDelete;

    @Column(name="created_by", nullable = true)
    private Long createdBy;

    @Column(name="created_date", nullable = true)
    private LocalDateTime createdOn;

    @Column(name="updated_by", nullable = true)
    private Long updatedBy;

    @Column(name="updated_date", nullable = true)
    private LocalDateTime updatedOn;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer gettEventId() {
        return tEventId;
    }

    public void settEventId(Integer tEventId) {
        this.tEventId = tEventId;
    }

    public Integer getRequestBy() {
        return requestBy;
    }

    public void setRequestBy(Integer requestBy) {
        this.requestBy = requestBy;
    }

    public LocalDateTime getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(LocalDateTime requestDate) {
        this.requestDate = requestDate;
    }

    public LocalDateTime getRequestDueDate() {
        return requestDueDate;
    }

    public void setRequestDueDate(LocalDateTime requestDueDate) {
        this.requestDueDate = requestDueDate;
    }

    public Integer getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(Integer approvedBy) {
        this.approvedBy = approvedBy;
    }

    public LocalDateTime getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(LocalDateTime approvedDate) {
        this.approvedDate = approvedDate;
    }

    public Integer getReceivedBy() {
        return receivedBy;
    }

    public void setReceivedBy(Integer receivedBy) {
        this.receivedBy = receivedBy;
    }

    public LocalDateTime getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(LocalDateTime receivedDate) {
        this.receivedDate = receivedDate;
    }

    public Integer getSettlementBy() {
        return settlementBy;
    }

    public void setSettlementBy(Integer settlementBy) {
        this.settlementBy = settlementBy;
    }

    public LocalDateTime getSettlementDate() {
        return settlementDate;
    }

    public void setSettlementDate(LocalDateTime settlementDate) {
        this.settlementDate = settlementDate;
    }

    public Integer getSettlementApprovedBy() {
        return settlementApprovedBy;
    }

    public void setSettlementApprovedBy(Integer settlementApprovedBy) {
        this.settlementApprovedBy = settlementApprovedBy;
    }

    public LocalDateTime getSettlementApprovedDate() {
        return settlementApprovedDate;
    }

    public void setSettlementApprovedDate(LocalDateTime settlementApprovedDate) {
        this.settlementApprovedDate = settlementApprovedDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getRejectReason() {
        return rejectReason;
    }

    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDateTime createdOn) {
        this.createdOn = createdOn;
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public LocalDateTime getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(LocalDateTime updatedOn) {
        this.updatedOn = updatedOn;
    }
}