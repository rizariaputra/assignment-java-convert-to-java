package com.xa.java.assignment.aria.marcomapps.repositories;

import java.util.List;

import com.xa.java.assignment.aria.marcomapps.models.MasterUnitModel;

import org.springframework.data.jpa.repository.JpaRepository;


public interface MasterUnitRepository extends JpaRepository<MasterUnitModel, Long> {

    public static String GetNameOfCreatorOrEditor() {
        return "Administrator";
    }

    List<MasterUnitModel> findByIsDelete(Boolean isDelete);
    
}