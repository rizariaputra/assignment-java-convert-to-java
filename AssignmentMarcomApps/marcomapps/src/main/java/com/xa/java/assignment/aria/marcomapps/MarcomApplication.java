package com.xa.java.assignment.aria.marcomapps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarcomApplication {

	public static void main(String[] args) {
		SpringApplication.run(MarcomApplication.class, args);
	}

}
