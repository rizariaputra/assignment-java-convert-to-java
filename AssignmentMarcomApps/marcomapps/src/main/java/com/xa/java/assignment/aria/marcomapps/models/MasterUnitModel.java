package com.xa.java.assignment.aria.marcomapps.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PostPersist;
import javax.persistence.Table;

@Entity
@Table(name="m_unit")
public class MasterUnitModel {
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", nullable = false, length = 11)
    private Long id;
    
    @Column(name="code", nullable = false, length = 50)
    private String code;

    @PostPersist
    public void GenerateUnitCode() {
        if(id.toString().length() == 1) code = ("UN000" + id);
        else if(id.toString().length() == 2) code = ("UN00" + id);
        else if(id.toString().length() == 3) code = ("UN0" + id);
        else if(id.toString().length() == 4) code = ("UN" + id);
    }
    
    @Column(name="name", nullable = false, length = 50)
    private String name;
    
    @Column(name="description", nullable = true, length = 255)
    private String description;
    
    @Column(name="is_delete", nullable = false)
    private Boolean isDelete;

    @Column(name="created_by", nullable = false, length = 50)
    private String createdBy;

    @Column(name="created_on", nullable = false)
    private LocalDateTime createdOn;

    @Column(name="updated_by", nullable = true, length = 50)
    private String updatedBy;

    @Column(name="updated_on", nullable = true)
    private LocalDateTime updatedOn;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public LocalDateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(LocalDateTime createdOn) {
		this.createdOn = createdOn;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public LocalDateTime getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(LocalDateTime updatedOn) {
		this.updatedOn = updatedOn;
	}



    



}