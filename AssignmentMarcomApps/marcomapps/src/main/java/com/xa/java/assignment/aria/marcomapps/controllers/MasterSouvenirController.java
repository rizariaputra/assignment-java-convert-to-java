package com.xa.java.assignment.aria.marcomapps.controllers;

import java.time.LocalDateTime;
import java.util.List;

import com.xa.java.assignment.aria.marcomapps.models.MasterSouvenirModel;
import com.xa.java.assignment.aria.marcomapps.models.MasterUnitModel;
import com.xa.java.assignment.aria.marcomapps.repositories.MasterSouvenirRepository;
import com.xa.java.assignment.aria.marcomapps.repositories.MasterUnitRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/souvenir")
public class MasterSouvenirController {
    @Autowired 
    private MasterSouvenirRepository masterSouvenirRepo;
    
    @Autowired 
	private MasterUnitRepository masterUnitRepo;

    @GetMapping("index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("/souvenir/index");
		List<MasterSouvenirModel> souvenir = this.masterSouvenirRepo.findByIsDelete(false);
		view.addObject("souvenir", souvenir);
			return view;
	}

	// CREATE

    @GetMapping("create")
	public ModelAndView create() {
		ModelAndView view = new ModelAndView("souvenir/create");
		MasterSouvenirModel souvenir = new MasterSouvenirModel();
        view.addObject("souvenir", souvenir);
        List<MasterUnitModel> unit = this.masterUnitRepo.findByIsDelete(false);
		view.addObject("unit", unit);
			return view;
    }
    
    @PostMapping("save")
	public ModelAndView save(@ModelAttribute MasterSouvenirModel souvenir, BindingResult result) {
		if(!result.hasErrors()) {
			souvenir.setCreatedBy(MasterSouvenirRepository.GetNameOfCreatorOrEditor());
			souvenir.setCreatedOn(LocalDateTime.now());
			souvenir.setIsDelete(false);
			this.masterSouvenirRepo.save(souvenir);
			return new ModelAndView("redirect:/souvenir/index");
		} else {
			return new ModelAndView("redirect:/error/500");
		}
    }

	//EDIT/UPDATE

    @GetMapping("edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("/souvenir/update");
		MasterSouvenirModel souvenir = this.masterSouvenirRepo.findById(id).orElse(null);
        view.addObject("souvenir", souvenir);
        List<MasterUnitModel> unit = this.masterUnitRepo.findByIsDelete(false);
		view.addObject("unit", unit);
			return view;
    }

    @PostMapping("editsave")
	public ModelAndView editsave(@ModelAttribute MasterSouvenirModel souvenir, BindingResult result) {
		MasterSouvenirModel updatedSouvenir = this.masterSouvenirRepo.findById(souvenir.getId()).orElse(null);
		if(!result.hasErrors()) {
			updatedSouvenir.setName(souvenir.getName());
            updatedSouvenir.setDescription(souvenir.getDescription());
			updatedSouvenir.setmUnitId(souvenir.getmUnitId());
			updatedSouvenir.setUpdatedBy(MasterSouvenirRepository.GetNameOfCreatorOrEditor());
			updatedSouvenir.setUpdatedOn(LocalDateTime.now());
			this.masterSouvenirRepo.save(updatedSouvenir);
			return new ModelAndView("redirect:/souvenir/index");
		} else {
			return new ModelAndView("redirect:/error/500");
		}
	}
	
	// DETAILS/VIEW

	@GetMapping("details/{id}")
	public ModelAndView details(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("/souvenir/details");
		MasterSouvenirModel souvenir = this.masterSouvenirRepo.findById(id).orElse(null);
        view.addObject("souvenir", souvenir);
        List<MasterUnitModel> unit = this.masterUnitRepo.findByIsDelete(false);
		view.addObject("unit", unit);
			return view;
    }
	
	// DELETE

    @GetMapping("delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("/souvenir/delete");
		MasterSouvenirModel souvenir = this.masterSouvenirRepo.findById(id).orElse(null);
		view.addObject("souvenir", souvenir);
			return view;
	}

	@PostMapping("deletesave")
	public ModelAndView deletesave(@ModelAttribute MasterSouvenirModel souvenir, BindingResult result) {
		MasterSouvenirModel updatedSouvenir = this.masterSouvenirRepo.findById(souvenir.getId()).orElse(null);
		if(!result.hasErrors()) {
			updatedSouvenir.setIsDelete(true);
			updatedSouvenir.setUpdatedBy(MasterSouvenirRepository.GetNameOfCreatorOrEditor());
			updatedSouvenir.setUpdatedOn(LocalDateTime.now());
			this.masterSouvenirRepo.save(updatedSouvenir);
			return new ModelAndView("redirect:/souvenir/index");
		} else {
			return new ModelAndView("redirect:/error/500");
		}
	}

    @RequestMapping(path = "/trigger-error", produces = MediaType.APPLICATION_JSON_VALUE)
    public void error500() throws Exception {
        throw new Exception();
    }

}