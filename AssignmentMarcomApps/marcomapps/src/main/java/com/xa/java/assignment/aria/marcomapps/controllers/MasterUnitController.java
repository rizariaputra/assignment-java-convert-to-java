package com.xa.java.assignment.aria.marcomapps.controllers;

import java.time.LocalDateTime;
import java.util.List;

import com.xa.java.assignment.aria.marcomapps.models.MasterUnitModel;
import com.xa.java.assignment.aria.marcomapps.repositories.MasterUnitRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/unit")
public class MasterUnitController {
    @Autowired 
	private MasterUnitRepository masterUnitRepo;

    @GetMapping("index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("/unit/index");
		List<MasterUnitModel> unit = this.masterUnitRepo.findByIsDelete(false);
		view.addObject("unit", unit);
			return view;
	}

	// CREATE

    @GetMapping("create")
	public ModelAndView create() {
		ModelAndView view = new ModelAndView("unit/create");
		MasterUnitModel unit = new MasterUnitModel();
		view.addObject("unit", unit);
			return view;
    }
    
    @PostMapping("save")
	public ModelAndView save(@ModelAttribute MasterUnitModel unit, BindingResult result) {
		if(!result.hasErrors()) {
			unit.setCreatedBy(MasterUnitRepository.GetNameOfCreatorOrEditor());
			unit.setCreatedOn(LocalDateTime.now());
			unit.setIsDelete(false);
			this.masterUnitRepo.save(unit);
			return new ModelAndView("redirect:/unit/index");
		} else {
			return new ModelAndView("redirect:/error/500");
		}
    }

	//EDIT/UPDATE

    @GetMapping("edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("/unit/update");
		MasterUnitModel unit = this.masterUnitRepo.findById(id).orElse(null);
		view.addObject("unit", unit);
			return view;
    }

    @PostMapping("editsave")
	public ModelAndView editsave(@ModelAttribute MasterUnitModel unit, BindingResult result) {
		MasterUnitModel updatedUnit = this.masterUnitRepo.findById(unit.getId()).orElse(null);
		if(!result.hasErrors()) {
			updatedUnit.setName(unit.getName());
			updatedUnit.setDescription(unit.getDescription());
			updatedUnit.setUpdatedBy(MasterUnitRepository.GetNameOfCreatorOrEditor());
			updatedUnit.setUpdatedOn(LocalDateTime.now());
			this.masterUnitRepo.save(updatedUnit);
			return new ModelAndView("redirect:/unit/index");
		} else {
			return new ModelAndView("redirect:/error/500");
		}
	}
	
	// DETAILS/VIEW

	@GetMapping("details/{id}")
	public ModelAndView details(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("/unit/details");
		MasterUnitModel unit = this.masterUnitRepo.findById(id).orElse(null);
		view.addObject("unit", unit);
			return view;
    }
	
	// DELETE

    @GetMapping("delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("/unit/delete");
		MasterUnitModel unit = this.masterUnitRepo.findById(id).orElse(null);
		view.addObject("unit", unit);
			return view;
	}

	@PostMapping("deletesave")
	public ModelAndView deletesave(@ModelAttribute MasterUnitModel unit, BindingResult result) {
		MasterUnitModel updatedUnit = this.masterUnitRepo.findById(unit.getId()).orElse(null);
		if(!result.hasErrors()) {
			updatedUnit.setIsDelete(true);
			updatedUnit.setUpdatedBy(MasterUnitRepository.GetNameOfCreatorOrEditor());
			updatedUnit.setUpdatedOn(LocalDateTime.now());
			this.masterUnitRepo.save(updatedUnit);
			return new ModelAndView("redirect:/unit/index");
		} else {
			return new ModelAndView("redirect:/error/500");
		}
	}

    @RequestMapping(path = "/trigger-error", produces = MediaType.APPLICATION_JSON_VALUE)
    public void error500() throws Exception {
        throw new Exception();
    }

}