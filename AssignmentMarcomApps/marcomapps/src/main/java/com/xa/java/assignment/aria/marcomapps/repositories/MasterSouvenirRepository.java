package com.xa.java.assignment.aria.marcomapps.repositories;

import java.util.List;

import com.xa.java.assignment.aria.marcomapps.models.MasterSouvenirModel;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MasterSouvenirRepository extends JpaRepository<MasterSouvenirModel, Long> {
    
    public static String GetNameOfCreatorOrEditor() {
        return "Administrator";
    }

    List<MasterSouvenirModel> findByIsDelete(Boolean isDelete);
}